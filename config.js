serverConfig = {
    hostname: "localhost",
    port: 5000,
    eventEmitter: null,
    event: "new-request"
}
const routerConfig = {
    eventEmitter: null,
    event: "new-request"
};

const contentTypes = {
    JSON: 'application/json',
    TEXT: 'text/html'
};

const statusCodes = {
    SUCCESS: 200,
    NOT_FOUND: 404,
    INTERNAL_ERROR: 500,
    UNAUTHORIZED: 401
};



const appsDirectory = './apps';

module.exports = {
    serverConfig,
    routerConfig,
    appsDirectory,
    contentTypes,
    statusCodes
}