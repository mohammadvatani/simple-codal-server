const dataParser = require('pionners-dataparser')
const {getCodal} = require('./controllers/codal')


module.exports.routes = {
    '/codal':{
        POST: {
          function: getCodal,
          middlewares: [dataParser]
        }
    }
}