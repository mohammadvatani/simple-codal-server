const c = require('../config');
const axios = require('axios');
const {sendFail, sendOk} = require('../../../utils/response-handler')

async function getCodal(req,res){
    const url = req.data.url
    try{
        response = await axios.get(url)
        return sendOk(res, response.data)
    }catch(e){
        console.log(e)
        return sendFail(res, c.statusCodes.NOT_FOUND, {message: c.errors.NOT_FOUND.title})
    }
}


module.exports = {
    getCodal
}